<?php

class Notification {

    // TEST

    public static function notification_get($data = []) {

        // var
        $where = '';
        // where
        if (isset($data['viewed']))
            $where = "WHERE viewed = ". (int) $data['viewed'];
        // info
        $q = DB::query("SELECT notification_id, user_id , title, description, viewed, created  FROM user_notifications ".$where." ") or die (DB::error());
        $row_array =  DB::fetch_all($q);
        if (!empty($row_array)) {
            return $row_array;
        } else {
            return [
                'id'        =>  0,
                'user_id'   => 0,
                'title'     => "",
                'viewed'    => 0,
                'created'   => 0,
            ];
        }
    }

    public static function notification_read() {
        // info
        $q = DB::query("SELECT notification_id, user_id , title, description, viewed, created  FROM user_notifications") or die (DB::error());
        $row_array =  DB::fetch_all($q);
        if (!empty($row_array)) {
            return $row_array;
        } else {
            return [
                'id'        =>  0,
                'user_id'   => 0,
                'title'     => "",
                'viewed'    => 0,
                'created'   => 0,
            ];
        }
    }

}
