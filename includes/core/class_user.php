<?php

class User {

    // GENERAL

    public static function user_info($data) {
        // vars
        $user_id = isset($data['user_id']) && is_numeric($data['user_id']) ? $data['user_id'] : 0;
        $phone = isset($data['phone']) ? preg_replace('~[^\d]+~', '', $data['phone']) : 0;
        // where
        if ($user_id) $where = "user_id='".$user_id."'";
        else if ($phone) $where = "phone='".$phone."'";
        else return [];
        // info
        $q = DB::query("SELECT user_id, first_name, last_name, middle_name, email, gender_id, count_notifications FROM users WHERE ".$where." LIMIT 1;") or die (DB::error());
        if ($row = DB::fetch_row($q)) {
            return [
                'id' => (int) $row['user_id'],
                'first_name' => $row['first_name'],
                'last_name' => $row['last_name'],
                'middle_name' => $row['middle_name'],
                'gender_id' => (int) $row['gender_id'],
                'email' => $row['email'],
                'phone' => (int) $row['phone'],
                'phone_str' => phone_formatting($row['phone']),
                'count_notifications' => (int) $row['count_notifications']
            ];
        } else {
            return [
                'id' => 0,
                'first_name' => '',
                'last_name' => '',
                'middle_name' => '',
                'gender_id' => 0,
                'email' => '',
                'phone' => '',
                'phone_str' => '',
                'count_notifications' => 0
            ];
        }
    }

    public static function user_get_or_create($phone) {
        // validate
        $user = User::user_info(['phone' => $phone]);
        $user_id = $user['id'];
        // create
        if (!$user_id) {
            DB::query("INSERT INTO users (status_access, phone, created) VALUES ('3', '".$phone."', '".Session::$ts."');") or die (DB::error());
            $user_id = DB::insert_id();
        }
        // output
        return $user_id;
    }

    // TEST

    public static function owner_info($data = []) {
       return self::user_info($data);
    }

    public static function owner_update($data = []) {
        // vars
        $allowed_fields = ['first_name', 'last_name', 'middle_name', 'email', 'phone'];
        $errors = [];
        $prepare_sql = "";
        $user_id = (int) $data['user_id'];

        // filter fields and make prepare sql string
        $filter_fields = [];
        foreach ($data as $key => $fields ){
            if (in_array($key,$allowed_fields)){
                $filter_fields[$key] = $data[$key];
                $prepare_sql .= $key."=:".$key.", ";
            }
        }

       // if empty all fields
        if (empty($filter_fields))
            return error_response(1003, 'One of the parameters was missing', $allowed_fields);

        // check for validate
        if (!$user_id)
            $errors[]  = 'The parameter "user_id" can not be empty.';

        if (isset($filter_fields['first_name']) && empty($filter_fields['first_name']))
            $errors[]  = 'The parameter "first_name" can not be empty.';

        if (isset($filter_fields['last_name']) && empty($filter_fields['last_name']))
            $errors[]  = 'The parameter "last_name" can not be empty.';

        if (isset($filter_fields['phone']) && empty($filter_fields['phone'])){
            $errors[]  = 'The parameter "phone" can not be empty.';

        }else if (isset($filter_fields['phone']) && !empty($filter_fields['phone'])){
            $phone_number = preg_replace("~[^\d]~", '', $filter_fields['phone']);
            if ($phone_number[0] != 7 || strlen($phone_number) != 11 )
                $errors[] = 'The parameter "phone" was passed in the wrong format.';
        }

        if (isset($filter_fields['email']))
            $filter_fields['email'] = mb_strtolower($filter_fields['email']);

        // validate
        if (!empty($errors))
            return error_response(1003, $errors);

        // update
        DB::prepare("UPDATE users SET ".trim($prepare_sql,", "). " WHERE user_id  = ".$user_id);
        DB::execute($filter_fields);

         if(DB::rowCount()){
             DB::query("INSERT INTO user_notifications (user_id, title, description, viewed, created) VALUES (".$user_id.",'The test task','The test task - description',0,'".time()."')") or die (DB::error());

             return response(['update' => 1]);
         }

        return response(['update' => 0]);

    }

}
